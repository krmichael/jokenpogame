import React, { Component } from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  AppRegistry
} from 'react-native';

import Topo from './src/components/Topo';
import Icone from './src/components/Icone';

export default class JokenpoGame extends Component {

  constructor(props) {
    super(props);

    this.state = {
      escolhaUser: '',
      escolhaPc: '',
      resultado: ''
    }
  }

  jokenpo = (escolhaUser) => {

    const numAleatorio = Math.floor(Math.random() * 3);
    let escolhaPc = '';
    let resultado = '';

    switch (numAleatorio) {
      case 0: escolhaPc = 'pedra'; break;
      case 1: escolhaPc = 'papel'; break;
      case 2: escolhaPc = 'tesoura'; break;
      default: escolhaPc = ''; break;
    }

    if (escolhaUser === 'pedra') {
      if (escolhaPc === 'pedra') {
        resultado = 'Empate!!';
      } else {
        resultado = 'Você ganhou!!';
      }
    }
    if (escolhaUser === 'papel') {
      if (escolhaPc === 'papel') {
        resultado = 'Empate!!';
      } else {
        resultado = 'Você perdeu!!';
      }
    }
    if (escolhaUser === 'tesoura') {
      if (escolhaPc === 'tesoura') {
        resultado = 'Empate!!';
      } else if (escolhaPc === 'papel') {
        resultado = 'Você ganhou!!';
      } else {
        resultado = 'Você perdeu!!';
      }
    }

    this.setState({escolhaUser, escolhaPc, resultado});
  }

  render() {

    const { scopebtns, btn, palco, txtResultado } = styles;

    return (
      <View>

        <Topo />

        <View style={scopebtns}>
          <View style={btn}>
            <Button onPress={() => this.jokenpo('pedra')} title="Pedra" />
          </View>

          <View style={btn}>
            <Button onPress={() => this.jokenpo('papel')} title="Papel" />
          </View>

          <View style={btn}>
            <Button onPress={() => this.jokenpo('tesoura')} title="Tesoura" />
          </View>
        </View>

        <View style={palco}>
          <Text style={txtResultado}>{this.state.resultado}</Text>

          <Icone escolha={this.state.escolhaUser} jogador="Você" />
          <Icone escolha={this.state.escolhaPc} jogador="Computador" />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scopebtns: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  btn: {
    width: 100
  },
  palco: {
    alignItems: 'center',
    marginTop: 10
  },
  txtResultado: {
    fontSize: 20,
    fontWeight: 'bold',
    minHeight: 50,
    color: '#2f2f2f'
  }
});

AppRegistry.registerComponent('JokenpoGame', () => JokenpoGame);