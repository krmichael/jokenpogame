import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';

const imgPedra = require('../../img/pedra.png');
const imgPapel = require('../../img/papel.png');
const imgTesoura = require('../../img/tesoura.png');

export default class Icone extends Component {
  render() {
    const { icone, txtIcone } = styles;

    if (this.props.escolha === 'pedra') {
      return (
        <View style={icone}>
          <Text style={txtIcone}>{this.props.jogador}</Text>
          <Image source={imgPedra} />
        </View>
      );
    } else if (this.props.escolha === 'papel') {
      return (
        <View style={icone}>
          <Text style={txtIcone}>{this.props.jogador}</Text>
          <Image source={imgPapel} />
        </View>
      );
    } else if (this.props.escolha === 'tesoura') {
      return (
        <View style={icone}>
          <Text style={txtIcone}>{this.props.jogador}</Text>
          <Image source={imgTesoura} />
        </View>
      );
    }
    return false; //Chamado no carregamento quando nenhum botão ainda foi clicado
  }
}

const styles = StyleSheet.create({
  icone: {
    alignItems: 'center',
    marginBottom: 10
  },
  txtIcone: {
    fontSize: 18
  }
});